const hb = require('handlebars');
const fs = require('fs-extra');
const path = require('path');
const express = require('express');
const cors = require('cors');

(async () => {
  const app = express()
  app.use(cors())
  app.use(express.static(path.join('public')))

  const quotation = {
    name: "Lucas Solares"
  }

  try {
    const fileTemplate = await fs.readFile(path.join('src', 'template.hbs'), 'utf-8')
    const template = hb.compile(fileTemplate)
    app.use('/template', (req, res) => res.send(template(quotation)))
  } catch {
    console.error("Ocurrio un error de compilacion")
  }

  app.listen(4000, () => {
    console.log('Server is Listening in http://localhost:4000/template')
  })
})()